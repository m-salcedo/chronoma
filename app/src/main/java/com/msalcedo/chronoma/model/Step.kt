package com.msalcedo.chronoma.model

/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/27/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
class Step {

    private val TAG = "TAG_${Step::class.java.simpleName}"

    var num: String = ""
    var step: String = ""
    var time: String = ""
    var mills: Long = 0L

    override fun toString(): String = "$num $step $time"
}