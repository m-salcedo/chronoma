package com.msalcedo.chronoma.ui

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.msalcedo.chronoma.R
import com.msalcedo.chronoma.ui.main.MainActivity

/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/29/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
class SplashActivity : AppCompatActivity() {

    private val TAG = "TAG_${SplashActivity::class.java.simpleName}"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            MainActivity.start(this)
            finish()
        }, 2000)
    }


}