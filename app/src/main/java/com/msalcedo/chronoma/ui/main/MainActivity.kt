package com.msalcedo.chronoma.ui.main


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.msalcedo.chronoma.R
import com.msalcedo.chronoma.helper.StepsAdapter
import com.msalcedo.chronoma.model.Step
import com.msalcedo.chronoma.ui.utils.HoloCircularProgressBar
import com.msalcedo.chronoma.ui.utils.MyChronometer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/27/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
class MainActivity : AppCompatActivity(), IMainView, StepsAdapter.OnStepsListener {

    private lateinit var presenter: MainPresenter
    private lateinit var adapter: StepsAdapter

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)

        fabCenter.setOnClickListener {
            presenter.onClickCenterButton()
        }

        ivRight.setOnClickListener {
            presenter.onClickRightButton()
        }

        ivLeft.setOnClickListener {
            presenter.onClickLeftButton()
        }

        initRecycler()
    }

    private fun initRecycler() {
        var linearLayout = LinearLayoutManager(getContext())
        linearLayout.orientation = LinearLayoutManager.VERTICAL
        linearLayout.reverseLayout = true;
        linearLayout.stackFromEnd = true;
        rvSteps.layoutManager = linearLayout
        rvSteps.setHasFixedSize(true)
        adapter = StepsAdapter(this)
        rvSteps.adapter = adapter
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun getChronometerView(): MyChronometer = myChronometerView

    override fun getProgressView(): HoloCircularProgressBar = progressView

    override fun getContext(): Context = this

    override fun addStep(step: Step) {
        rvSteps.smoothScrollToPosition(adapter.itemCount)
        adapter.add(step)
    }

    override fun showButtonPause() {
        fabCenter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pause))
    }

    override fun showButtonPlay() {
        fabCenter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_arrow))
    }

    override fun showButtonStep() {
        ivLeft.visibility = View.VISIBLE
        ivLeft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_add))
    }

    override fun showButtonStop() {
        ivLeft.visibility = View.VISIBLE
        ivLeft.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_clear))
    }

    override fun hideButtonLeft() {
        ivLeft.visibility = View.GONE
    }

    override fun showButtonShare() {
        ivRight.visibility = View.VISIBLE
    }

    override fun hideButtonShare() {
        ivRight.visibility = View.GONE
    }

    override fun hideSteps() {
        llSteps.visibility = View.GONE
    }

    override fun showSteps() {
        llSteps.visibility = View.VISIBLE
    }

    override fun getItemSize(): Int = adapter.itemCount

    override fun getLastTime(): Long {
        return adapter.getLastTime()
    }

    override fun clearAdapter() {
        adapter.clear()
    }

    override fun deleteLastStep() {
        adapter.deleteLastStep()
    }

    override fun getStepString(): String = adapter.getStepsStr()

    override fun share(sharingIntent: Intent) {
        ContextCompat.startActivity(this, sharingIntent, null)
    }

}
