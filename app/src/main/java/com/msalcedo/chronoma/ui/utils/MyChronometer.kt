package com.msalcedo.chronoma.ui.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import android.os.SystemClock
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.View
import android.widget.TextView


/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/29/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
class MyChronometer : TextView {

    private val TAG = "TAG_${MyChronometer::class.java.simpleName}"

    private var base: Long = 0
    private var visible: Boolean = false
    private var started: Boolean = false
    private var running1: Boolean = false
    private var onChronometerTickListener: OnChronometerTickListener? = null

    private val TICK_WHAT = 2

    private var timeElapsed: Long = 0

    companion object {
        fun formatTime(time: Long): String {
            val hour = (time / 3600000)
            val minutes = (time - hour * 3600000) / 60000
            val seconds = (time - hour * 3600000 - minutes * 60000) / 1000
            val milliseconds = (time - hour * 3600000 - minutes * 60000 - seconds * 1000)
            return (if (minutes < 10) "0" + minutes else "" + minutes) + ":" + (if (seconds < 10) "0" + seconds else seconds) +
                    ":" + (if (milliseconds < 100) "0" + milliseconds else if (milliseconds < 10) "00" + milliseconds else milliseconds)
        }
    }

    constructor(context: Context) : super(context, null as AttributeSet?, 0)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        base = SystemClock.elapsedRealtime()
        updateText(base)
    }


    fun setBase(base: Long) {
        this.base = base
        dispatchChronometerTick()
        updateText(SystemClock.elapsedRealtime())
    }

    fun getBase(): Long {
        return base
    }

    fun setOnChronometerTickListener(
            listener: OnChronometerTickListener) {
        onChronometerTickListener = listener
    }

    fun getOnChronometerTickListener(): OnChronometerTickListener? {
        return onChronometerTickListener
    }

    fun start() {
        started = true
        updateRunning()
    }

    fun stop() {
        started = false
        updateRunning()
    }


    fun setStarted(started: Boolean) {
        this.started = started
        updateRunning()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        visible = false
        updateRunning()
    }

    override fun onWindowVisibilityChanged(visibility: Int) {
        super.onWindowVisibilityChanged(visibility)
        visible = visibility == View.VISIBLE
        updateRunning()
    }

    @Synchronized private fun updateText(now: Long) {
        timeElapsed = now - base

        val s = formatTime(timeElapsed)
        val ss1 = SpannableString(s)
        ss1.setSpan(RelativeSizeSpan(2f), 0, 5, 0) // set size

        text = ss1
    }

    private fun updateRunning() {
        val running = visible && started
        if (running != running1) {
            if (running) {
                updateText(SystemClock.elapsedRealtime())
                dispatchChronometerTick()
                mHandler.sendMessageDelayed(Message.obtain(mHandler,
                        TICK_WHAT), 100)
            } else {
                mHandler.removeMessages(TICK_WHAT)
            }
            running1 = running
        }
    }

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(m: Message) {
            if (running1) {
                updateText(SystemClock.elapsedRealtime())
                dispatchChronometerTick()
                sendMessageDelayed(Message.obtain(this, TICK_WHAT),
                        100)
            }
        }
    }

    fun dispatchChronometerTick() {
        onChronometerTickListener?.onChronometerTick(this)
    }

    fun getTimeElapsed(): Long {
        return timeElapsed
    }


    interface OnChronometerTickListener {

        fun onChronometerTick(chronometer: MyChronometer)
    }


}