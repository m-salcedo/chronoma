package com.msalcedo.chronoma.ui.main

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.SystemClock
import android.support.v4.content.ContextCompat
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.msalcedo.chronoma.R
import com.msalcedo.chronoma.model.Step
import com.msalcedo.chronoma.ui.utils.HoloCircularProgressBar
import com.msalcedo.chronoma.ui.utils.MyChronometer


/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/27/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
class MainPresenter(private val mView: IMainView) {

    private val TAG = "TAG_${MainPresenter::class.java.simpleName}"

    private var mProgressBarAnimator: ObjectAnimator? = null

    private var mAnimationTime: Long = 0

    private var mProgressTime: Float = 0.0f

    private var timeWhenStopped: Long

    private var mStatus: STATUS = STATUS.STOP

    private var preview: Boolean = false

    enum class STATUS {
        PLAY,
        PAUSE,
        STOP
    }

    init {

        mView.getChronometerView().setBase(SystemClock.elapsedRealtime())

        mAnimationTime = 0

        timeWhenStopped = 0

        mView.hideButtonShare()

        mView.hideButtonLeft()
    }

    fun onResume() {

    }

    fun onClickCenterButton() {
        if (mStatus == STATUS.STOP || mStatus == STATUS.PAUSE) {
            play()
        } else {
            pause()
        }
    }

    fun onClickRightButton() {
        share()
    }

    fun onClickLeftButton() {
        if (mStatus.equals(STATUS.PAUSE)) {
            clear()
        } else {
            addStep()
        }
    }

    private fun play() {
        mStatus = STATUS.PLAY
        mView.showButtonStep()
        mView.showButtonPause()
        mView.hideButtonShare()

        mView.getChronometerView().setBase(SystemClock.elapsedRealtime() + timeWhenStopped)
        mView.getChronometerView().start()

        if (preview) {
            mView.deleteLastStep()
            preview = false
        }

        playAnimation()
    }

    private fun pause() {
        mStatus = STATUS.PAUSE
        mView.showButtonPlay()
        mView.showButtonStop()

        mView.showButtonShare()

        timeWhenStopped = (mView.getChronometerView().getBase() - SystemClock.elapsedRealtime())
        mView.getChronometerView().stop()

        pauseAnimation()

        if (mView.getItemSize() > 0) {
            preview = true
            addStep()
        }
    }

    private fun clear() {
        mStatus = STATUS.STOP

        mView.getChronometerView().setBase(SystemClock.elapsedRealtime())

        mAnimationTime = 0

        timeWhenStopped = 0

        mProgressTime = 0f

        mView.hideButtonShare()

        mView.hideButtonLeft()

        mView.hideSteps()

        mView.clearAdapter()

        clearAnimation()

        preview = false
    }

    private fun addStep() {
        mView.showSteps()

        val millisTime = (SystemClock.elapsedRealtime() - mView.getChronometerView().getBase())

        val millisStep = (SystemClock.elapsedRealtime() - mView.getChronometerView().getBase()) - mView.getLastTime()

        val step = Step()

        val hmsTime = MyChronometer.formatTime(millisTime)

        val hmsStep = MyChronometer.formatTime(millisStep)

        step.num = String.format(mView.getContext().getString(R.string.format_num), mView.getItemSize() + 1)

        step.step = hmsStep

        step.time = hmsTime

        step.mills = millisTime

        mView.addStep(step)
    }

    private fun share() {
        val millsTime = mView.getLastTime()

        val timeString = MyChronometer.formatTime(millsTime)

        var shareBody = String.format(mView.getContext().getString(R.string.title_share), timeString) + "\n"

        if (mView.getItemSize() > 0) {

            shareBody += mView.getContext().getString(R.string.body_share) + mView.getStepString()
        }

        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)

        sharingIntent.type = "text/plain"

        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mView.getContext().getString(R.string.app_name))

        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)

        mView.share(sharingIntent)
    }

    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    // Animate
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------

    private fun pauseAnimation() {

        if (mProgressBarAnimator != null) {
            mAnimationTime = mProgressBarAnimator!!.getCurrentPlayTime()
            mProgressTime = mView.getProgressView().progress
            mProgressBarAnimator!!.cancel()
            mProgressBarAnimator!!.end()

        }
        mView.getProgressView().progressColor = ContextCompat.getColor(mView.getContext(), R.color.colorPaused)
        mView.getProgressView().setProgressBackgroundColor(ContextCompat.getColor(mView.getContext(), R.color.colorPaused))
        startBlink()
    }

    private fun playAnimation() {
        if (mProgressBarAnimator != null) {
            mProgressBarAnimator!!.start()
            mProgressBarAnimator!!.currentPlayTime = mAnimationTime
            mView.getProgressView().progress = mProgressTime
            mView.getProgressView().progressColor = ContextCompat.getColor(mView.getContext(), R.color.colorAccent)
            mView.getProgressView().setProgressBackgroundColor(ContextCompat.getColor(mView.getContext(), R.color.colorAccent))
        } else {
            animate(mView.getProgressView(), null, 1f, 1000)
        }
        stopBlink()
    }

    private fun clearAnimation() {
        mProgressBarAnimator!!.end()

        mProgressBarAnimator = null

        mView.getProgressView().progressColor = ContextCompat.getColor(mView.getContext(), R.color.colorAccent)

        mView.getProgressView().setProgressBackgroundColor(ContextCompat.getColor(mView.getContext(), R.color.white))

        stopBlink()
    }

    private fun animate(progressBar: HoloCircularProgressBar, listener: AnimatorListener?,
                        progress: Float, duration: Long) {

        mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress", progress)
        mProgressBarAnimator!!.setDuration(duration)

        mProgressBarAnimator!!.addListener(object : AnimatorListener {

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationEnd(animation: Animator) {
                progressBar.progress = mProgressTime
            }

            override fun onAnimationRepeat(animation: Animator) {
                mView.getProgressView().setProgressBackgroundColor(ContextCompat.getColor(mView.getContext(), R.color.colorAccent))
            }

            override fun onAnimationStart(animation: Animator) {}
        })
        if (listener != null) {
            mProgressBarAnimator!!.addListener(listener)
        }
        mProgressBarAnimator!!.reverse()
        mProgressBarAnimator!!
                .addUpdateListener({ animation -> progressBar.progress = animation.animatedValue as Float })
        progressBar.markerProgress = progress
        mProgressBarAnimator!!.repeatCount = ObjectAnimator.INFINITE
        mProgressBarAnimator!!.start()
    }

    private fun startBlink() {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 500
        anim.startOffset = 20
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        mView.getChronometerView().startAnimation(anim)
    }

    private fun stopBlink() {
        mView.getChronometerView().clearAnimation()
    }
}