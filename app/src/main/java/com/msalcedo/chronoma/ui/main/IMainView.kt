package com.msalcedo.chronoma.ui.main

import android.content.Context
import android.content.Intent
import com.msalcedo.chronoma.model.Step
import com.msalcedo.chronoma.ui.utils.HoloCircularProgressBar
import com.msalcedo.chronoma.ui.utils.MyChronometer

/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/27/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
interface IMainView {
    fun getChronometerView(): MyChronometer
    fun getProgressView(): HoloCircularProgressBar
    fun getContext(): Context
    fun addStep(step: Step)
    fun showButtonPause()
    fun showButtonPlay()
    fun showButtonStep()
    fun showButtonStop()
    fun hideButtonLeft()
    fun showButtonShare()
    fun hideButtonShare()
    fun showSteps()
    fun hideSteps()
    fun getItemSize(): Int
    fun getLastTime(): Long
    fun clearAdapter()
    fun deleteLastStep()
    fun getStepString(): String
    fun share(sharingIntent: Intent)

}