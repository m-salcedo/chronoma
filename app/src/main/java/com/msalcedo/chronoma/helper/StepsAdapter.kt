package com.msalcedo.chronoma.helper

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.msalcedo.chronoma.R
import com.msalcedo.chronoma.model.Step
import java.util.*

/**
 * Created by Mariangela Salcedo (msalcedo047@gmail.com) on 11/27/17.
 * Copyright (c) 2017 m-salcedo. All rights reserved.
 */
class StepsAdapter(listener: OnStepsListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "TAG_${StepsAdapter::class.java.simpleName}"

    private var mItems: ArrayList<Step> = ArrayList()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val holder = holder as ViewHolder
        val step = mItems[position]

        holder.tvNum!!.text = step.num
        holder.tvStep!!.text = step.step
        holder.tvTime!!.text = step.time
    }

    override fun getItemCount(): Int = mItems.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val vi = LayoutInflater.from(parent!!.context)
        val v: View
        v = vi.inflate(R.layout.item_steps, parent, false)
        return ViewHolder(v)
    }

    interface OnStepsListener

    class ViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        private val root = view
        val tvNum: TextView? = root?.findViewById(R.id.tvNum)
        val tvStep: TextView? = root?.findViewById(R.id.tvStep)
        val tvTime: TextView? = root?.findViewById(R.id.tvTime)
    }

    fun add(step: Step) {
        mItems.add(step)
        notifyDataSetChanged()
    }

    fun getLastTime(): Long {
        return if (mItems.isEmpty()) {
            0L
        } else {
            mItems[mItems.size - 1].mills
        }
    }

    fun clear() {
        mItems.clear()
        notifyDataSetChanged()
    }

    fun deleteLastStep() {
        mItems.removeAt(mItems.size - 1)
        notifyDataSetChanged()
    }

    fun getStepsStr(): String {
        var str = ""
        for (step: Step in mItems) {
            str += (step.toString() + "\n")
        }

        return str
    }
}