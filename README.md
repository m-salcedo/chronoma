# chronoma

Contents table
-----------------

- [Apk installable - link](https://drive.google.com/open?id=1-FC6yN-s26IumS-1a366yQKyvJ8jfENH)
- [Technical specifications](#technical-specifications)
- [Structure](#structure)
- [Version control](#version)
- [Libraries](#libraries)


Technical specifications
-------------

- [Example test app](https://drive.google.com/file/d/0B_usVBysSyOuWU9JaXZpMndQYkk/view)


Structure
---------------

- [Kotlin Android] (https://developer.android.com/guide/index.html?hl=es-419)
- [MVP (Model - View - Presenter)] (https://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)


Version
---------------

        // Versions of the build
        min_sdk_version = '17'
        target_sdk_version = '26'
        compile_sdk_version = 26

        // Gradle
        gradle_version = '3.0.0'


Libraries
---------------

```bash
dependencies {

    implementation fileTree(dir: 'libs', include: ['*.jar'])

    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation"org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version"
    implementation 'com.android.support:appcompat-v7:26.1.0'
    implementation 'com.android.support.constraint:constraint-layout:1.0.2'
    implementation 'com.android.support:design:26.1.0'
    implementation 'uk.co.chrisjenx:calligraphy:2.3.0'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.android.support.test:runner:1.0.1'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.1'

}
